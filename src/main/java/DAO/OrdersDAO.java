package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

import Bll.ClientBll;
import Bll.ProductBll;
import Connection.ConnectorJ;
import model.Client;
import model.Product;
import model.Orders;


public class OrdersDAO {
    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO orders (idOrder,idClient,idProduct,quantity)"
            + " VALUES (?,?,?,?)";
    private final static String findStatementString = "SELECT * FROM orders where idOrder = ?";
    private static String updateStatementString = "UPDATE orders SET idClient = ?, idProduct = ? , quantity = ? WHERE idOrder =";
    private  static String deleteStatementString = "DELETE FROM orders WHERE idOrder =";
    public static Orders findById(int orderId) {
        Orders toReturn = null;

        Connection dbConnection = ConnectorJ.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, orderId);
            rs = findStatement.executeQuery();
            rs.next();

            int idC = rs.getInt("idClient");
            int idP = rs.getInt("idProduct");
            int q = rs.getInt("quantity");

            toReturn = new Orders(orderId,idC,idP,q);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
        } finally {
            ConnectorJ.close(rs);
            ConnectorJ.close(findStatement);
            ConnectorJ.close(dbConnection);
        }
        return toReturn;
    }

    public static ArrayList<Orders>  findAll() {
        int idO;
        ArrayList<Orders> all=new ArrayList<>();
        Orders toReturn = null;

        Connection dbConnection = ConnectorJ.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try {

            findStatement = dbConnection.prepareStatement("SELECT * FROM orders");
            //findStatement.setLong(1,idClient);
            rs = findStatement.executeQuery();
            // rs.next();
            while(rs.next()) {

                idO=rs.getInt("idOrder");
                int idC=rs.getInt("idClient");
                int idP=rs.getInt("idProduct");
                int q=rs.getInt("quantity");
                toReturn = new Orders(idO,idC,idP,q);
                all.add(toReturn);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrdersDAO:findById " + e.getMessage());
        }
        finally {
            ConnectorJ.close(rs);
            ConnectorJ.close(findStatement);
            ConnectorJ.close(dbConnection);
        }
        return all;
    }

    public static int insert(Orders ord) {
        Connection dbConnection = ConnectorJ.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            Client cl = ClientDAO.findById(ord.getIdClient());
            Product pr = ProductDAO.findById(ord.getIdProduct());
            if (cl == null || pr==null) {
                throw new NoSuchElementException("The client or product with id =" + ord.getIdClient() + " doesn't exist!");
            }
            if(ord.getQuantity()>pr.getStock()) {
                throw new NoSuchElementException("The quantity for the product with id =" + ord.getIdProduct() + " is too much!");
            }
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);

            insertStatement.setInt(1, ord.getIdOrder());
            insertStatement.setInt(2,ord.getIdClient());
            insertStatement.setInt(3, ord.getIdProduct());
            insertStatement.setInt(4,ord.getQuantity());
            insertStatement.executeUpdate();
            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrdersDAO:insert " + e.getMessage());
        } finally {
            ConnectorJ.close(insertStatement);
            ConnectorJ.close(dbConnection);
        }
        return insertedId;
    }
    public static int Update(int id,Orders ord) {

        Connection dbConnection = ConnectorJ.getConnection();

        PreparedStatement updateStatement = null;
        int UpdatedId = -1;
        try {
            updateStatementString+=Integer.toString(id);
            Client cl = ClientDAO.findById(ord.getIdClient());
            Product pr = ProductDAO.findById(ord.getIdProduct());
            ProductBll prUp=new ProductBll();
            if (cl == null || pr==null) {
                throw new NoSuchElementException("The client or product with id =" + ord.getIdClient() + " doesn't exist!");
            }
            if(ord.getQuantity()>pr.getStock()) {
                throw new NoSuchElementException("The quantity for the product with id =" + ord.getIdProduct() + " is too much!");
            }
            updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);

            updateStatement.setInt(1,ord.getIdClient());
            updateStatement.setInt(2, ord.getIdProduct());
            updateStatement.setInt(3, ord.getQuantity());
            pr.setStock(pr.getStock()-ord.getQuantity());   //Aici updatez sotck ul in tabel dupa ce s a dat comanda
            prUp.updateProduct(ord.getIdProduct(),pr);
            updateStatement.executeUpdate();
            ResultSet rs = updateStatement.getGeneratedKeys();
            if (rs.next()) {
                UpdatedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrdersDAO:insert " + e.getMessage());
        } finally {
            ConnectorJ.close(updateStatement);
            ConnectorJ.close(dbConnection);
        }
    String updateStatementString = "UPDATE orders SET idClient = ?, idProduct = ? , quantity = ? WHERE idOrder =";
        return UpdatedId;
    }
    public static int Delete(int id) {

        Connection dbConnection = ConnectorJ.getConnection();

        PreparedStatement deleteStatement = null;
        int DeletedId = -1;
        try {
            deleteStatementString+=Integer.toString(id);
            deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
            deleteStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:delete " + e.getMessage());
        } finally {
            ConnectorJ.close(deleteStatement);
            ConnectorJ.close(dbConnection);
        }
        deleteStatementString = "DELETE FROM orders WHERE idOrder =";
        return DeletedId;
    }
}
