package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import Connection.ConnectorJ;
import model.Client;


public class ClientDAO {

    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO client (idClient,numeClient,telefon,adresa,email,varsta)"
            + " VALUES (?,?,?,?,?,?)";
    private final static String findStatementString = "SELECT * FROM client where idClient = ?";
    private static String updateStatementString = "UPDATE client SET numeClient = ?, telefon = ?, adresa = ?, email = ?, varsta= ? WHERE idClient =";
    private  static String deleteStatementString = "DELETE FROM client WHERE idClient =";
    public static Client findById(int clientId) {
        Client toReturn = null;

        Connection dbConnection = ConnectorJ.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, clientId);
            rs = findStatement.executeQuery();
            rs.next();

            String name = rs.getString("numeClient");
            String address = rs.getString("adresa");
            String email = rs.getString("email");
            int age = rs.getInt("varsta");
            String telefon=rs.getString("telefon");
            toReturn = new Client(clientId, name, address, email, age,telefon);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"StudentDAO:findById " + e.getMessage());
        } finally {
            ConnectorJ.close(rs);
            ConnectorJ.close(findStatement);
            ConnectorJ.close(dbConnection);
        }
        return toReturn;
    }

    public static ArrayList<Client>  findAll() {
        int idClient=1;
        ArrayList<Client> all=new ArrayList<Client>();
        Client toReturn = null;

        Connection dbConnection = ConnectorJ.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;

            try {

                findStatement = dbConnection.prepareStatement("SELECT * FROM client");
                //findStatement.setLong(1,idClient);
                rs = findStatement.executeQuery();
               // rs.next();
                while(rs.next()!=false) {

                     idClient=rs.getInt("idClient");
                    String name = rs.getString("numeClient");
                    String telefon=rs.getString("telefon");
                    String address = rs.getString("adresa");
                    String email = rs.getString("email");
                    int age = rs.getInt("varsta");
                    toReturn = new Client(idClient, name, address, email, age,telefon);
                    all.add(toReturn);
                }
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, "StudentDAO:findById " + e.getMessage());
            }
                finally {
                ConnectorJ.close(rs);
                ConnectorJ.close(findStatement);
                ConnectorJ.close(dbConnection);
            }

        return all;
    }


    public static int insert(Client client) {
        Connection dbConnection = ConnectorJ.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setInt(1, client.getId());
            insertStatement.setString(2, client.getName());
            insertStatement.setString(3, client.getTelefon());
            insertStatement.setString(4, client.getAddress());
            insertStatement.setString(5, client.getEmail());
            insertStatement.setInt(6, client.getAge());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "StudentDAO:insert " + e.getMessage());
        } finally {
            ConnectorJ.close(insertStatement);
            ConnectorJ.close(dbConnection);
        }
        return insertedId;
    }
    public static int Update(int id,Client client) {

        Connection dbConnection = ConnectorJ.getConnection();

        PreparedStatement updateStatement = null;
        int UpdatedId = -1;
        try {
            updateStatementString+=Integer.toString(id);
            updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
            updateStatement.setString(1, client.getName());
            updateStatement.setString(2, client.getTelefon());
            updateStatement.setString(3, client.getAddress());
            updateStatement.setString(4, client.getEmail());
            updateStatement.setInt(5, client.getAge());
            updateStatement.executeUpdate();

            ResultSet rs = updateStatement.getGeneratedKeys();
            if (rs.next()) {
                UpdatedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "StudentDAO:insert " + e.getMessage());
        } finally {
            ConnectorJ.close(updateStatement);
            ConnectorJ.close(dbConnection);
        }
        updateStatementString = "UPDATE client SET numeClient = ?, telefon = ?, adresa = ?, email = ?, varsta= ? WHERE idClient =";
        return UpdatedId;
    }

    public static int Delete(int id) {

        Connection dbConnection = ConnectorJ.getConnection();

        PreparedStatement deleteStatement = null;
        int DeletedId = -1;
        try {
            deleteStatementString+=Integer.toString(id);
            deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
            deleteStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "StudentDAO:delete " + e.getMessage());
        } finally {
            ConnectorJ.close(deleteStatement);
            ConnectorJ.close(dbConnection);
        }
        deleteStatementString = "DELETE FROM client WHERE idClient =";
        return DeletedId;
    }

}
