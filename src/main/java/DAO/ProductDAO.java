package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import Connection.ConnectorJ;
import model.Client;
import model.Product;

public class ProductDAO {
    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO product (id,name,price,stock)"
            + " VALUES (?,?,?,?)";
    private final static String findStatementString = "SELECT * FROM product where id = ?";
    private static String updateStatementString = "UPDATE product SET name = ?, price = ?, stock = ? WHERE id =";
    private  static String deleteStatementString = "DELETE FROM product WHERE id =";
    public static Product findById(int productId) {
       Product toReturn = null;

        Connection dbConnection = ConnectorJ.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, productId);
            rs = findStatement.executeQuery();
            rs.next();

            String name = rs.getString("name");
            int price = rs.getInt("price");
            int stock = rs.getInt("stock");
            toReturn = new Product(productId,name,price,stock);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
        } finally {
            ConnectorJ.close(rs);
            ConnectorJ.close(findStatement);
            ConnectorJ.close(dbConnection);
        }
        return toReturn;
    }

    public static ArrayList<Product>  findAll() {
        int idProduct;
        ArrayList<Product> all=new ArrayList<Product>();
        Product toReturn = null;

        Connection dbConnection = ConnectorJ.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try {

            findStatement = dbConnection.prepareStatement("SELECT * FROM product");
            //findStatement.setLong(1,idClient);
            rs = findStatement.executeQuery();
            // rs.next();
            while(rs.next()!=false) {

                idProduct=rs.getInt("id");
                String name = rs.getString("name");
                int price=rs.getInt("price");
               int stock=rs.getInt("stock");
                toReturn = new Product(idProduct,name,price,stock);
                all.add(toReturn);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:findById " + e.getMessage());
        }
        finally {
            ConnectorJ.close(rs);
            ConnectorJ.close(findStatement);
            ConnectorJ.close(dbConnection);
        }
        return all;
    }


    public static int insert(Product prod) {
        Connection dbConnection = ConnectorJ.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setInt(1, prod.getId());
            insertStatement.setString(2, prod.getName());
            insertStatement.setInt(3, prod.getPrice());
            insertStatement.setInt(4, prod.getStock());
            insertStatement.executeUpdate();
            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
        } finally {
            ConnectorJ.close(insertStatement);
            ConnectorJ.close(dbConnection);
        }
        return insertedId;
    }
    public static int Update(int id,Product prod) {

        Connection dbConnection = ConnectorJ.getConnection();

        PreparedStatement updateStatement = null;
        int UpdatedId = -1;
        try {
            updateStatementString+=Integer.toString(id);
            updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);

            updateStatement.setString(1, prod.getName());
            updateStatement.setInt(2, prod.getPrice());
            updateStatement.setInt(3, prod.getStock());
            updateStatement.executeUpdate();
            ResultSet rs = updateStatement.getGeneratedKeys();
            if (rs.next()) {
                UpdatedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
        } finally {
            ConnectorJ.close(updateStatement);
            ConnectorJ.close(dbConnection);
        }
        updateStatementString = "UPDATE product SET name = ?, price = ?, stock = ? WHERE id =";
        return UpdatedId;
    }
    public static int Delete(int id) {

        Connection dbConnection = ConnectorJ.getConnection();

        PreparedStatement deleteStatement = null;
        int DeletedId = -1;
        try {
            deleteStatementString+=Integer.toString(id);
            deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
            deleteStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:delete " + e.getMessage());
        } finally {
            ConnectorJ.close(deleteStatement);
            ConnectorJ.close(dbConnection);
        }
        deleteStatementString = "DELETE FROM product WHERE id =";
        return DeletedId;
    }
}
