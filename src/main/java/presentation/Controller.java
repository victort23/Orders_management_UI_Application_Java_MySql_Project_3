package presentation;

import javax.swing.*;

import Bll.ClientBll;
import Bll.OrdersBll;
import Bll.ProductBll;
import com.mysql.cj.jdbc.ClientInfoProviderSP;
import com.mysql.cj.x.protobuf.MysqlxCursor;
import model.Client;
import model.Orders;
import model.Product;
import presentation.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

    View view=new View();
    boolean clTable,OrdTable,PrdTable;
    public Controller() {
        view.TableSelected();
        view.Clients.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                view.createOptions("Clienti");
                    view.back.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            view.ClientiInteraction.dispose();
                            view.ClientiInteraction.setVisible(false);
                        }
                    });
               /* view.back.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        view.SpecificOp.dispose();
                        view.SpecificOp.setVisible(false);

                    }
                });*/
                    view.ADD.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            view.AddClientOperation();
                            view.back.addActionListener(new java.awt.event.ActionListener() {
                                public void actionPerformed(ActionEvent e) {
                                    view.SpecificOp.dispose();
                                    view.SpecificOp.setVisible(false);

                                    view.SpecificOp.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                                }
                            });
                            view.process.addActionListener(new java.awt.event.ActionListener() {
                                public void actionPerformed(ActionEvent e) {

                                    int id = Integer.valueOf(view.addId.getText());
                                    String name = view.addNume.getText();
                                    String email = view.addEmail.getText();
                                    String telefon = view.addTelefon.getText();
                                    int varsta = Integer.valueOf(view.addVarsta.getText());
                                    String adresa = view.addAdresa.getText();
                                    Client cl = new Client(id, name, adresa, email, varsta, telefon);
                                    ClientBll clientBll = new ClientBll();
                                    clientBll.insertClient(cl);
                                    view.back.addActionListener(new java.awt.event.ActionListener() {
                                        public void actionPerformed(ActionEvent e) {
                                            view.SpecificOp.dispose();
                                            view.SpecificOp.setVisible(false);

                                        }
                                    });
                                }
                            });
                        }
                    });
                    view.UPDATE.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            view.UpdateClientOperation();
                            view.process.addActionListener(new java.awt.event.ActionListener() {
                                public void actionPerformed(ActionEvent e) {

                                    int id = Integer.valueOf(view.addId.getText());
                                    String name = view.addNume.getText();
                                    String email = view.addEmail.getText();
                                    String telefon = view.addTelefon.getText();
                                    int varsta = Integer.valueOf(view.addVarsta.getText());
                                    String adresa = view.addAdresa.getText();
                                    Client cl = new Client(name, adresa, email, varsta, telefon);
                                    ClientBll clientBll = new ClientBll();
                                    clientBll.updateClient(id, cl);

                                    view.back.addActionListener(new java.awt.event.ActionListener() {
                                        public void actionPerformed(ActionEvent e) {
                                            view.SpecificOp.dispose();
                                            view.SpecificOp.setVisible(false);

                                        }
                                    });
                                }
                            });
                            view.back.addActionListener(new java.awt.event.ActionListener() {
                                public void actionPerformed(ActionEvent e) {
                                    view.SpecificOp.dispose();
                                    view.SpecificOp.setVisible(false);

                                }
                            });
                        }
                    });
                    view.DELETE.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            view.addId.resetKeyboardActions();
                            view.DeleteClientOperation();
                            view.process.addActionListener(new java.awt.event.ActionListener() {
                                public void actionPerformed(ActionEvent e) {

                                    int id = Integer.valueOf(view.addId.getText());
                                    ClientBll clientBll = new ClientBll();
                                    clientBll.Delete(id);
                                    view.back.addActionListener(new java.awt.event.ActionListener() {
                                        public void actionPerformed(ActionEvent e) {
                                            view.SpecificOp.dispose();
                                            view.SpecificOp.setVisible(false);

                                            view.SpecificOp.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                                        }
                                    });
                                }
                            });
                            view.back.addActionListener(new java.awt.event.ActionListener() {
                                public void actionPerformed(ActionEvent e) {
                                    view.SpecificOp.dispose();
                                    view.SpecificOp.setVisible(false);

                                    view.SpecificOp.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                                }
                            });
                        }
                    });
                    view.findById.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            view.FindByIdClientOperation();
                            view.back.addActionListener(new java.awt.event.ActionListener() {
                                public void actionPerformed(ActionEvent e) {
                                    view.SpecificOp.dispose();
                                    view.SpecificOp.setVisible(false);
                                }
                            });
                        }
                    });
                    view.showAll.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            view.createTableClient();
                            view.back.addActionListener(new java.awt.event.ActionListener() {
                                public void actionPerformed(ActionEvent e) {
                                    view.f1.dispose();
                                    view.f1.setVisible(false);
                                }
                            });
                        }
                    });
                }
        });
        view.Orders.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                view.createOptions("Order");

                    view.ADD.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            view.AddOrderOperation();

                            view.back.addActionListener(new java.awt.event.ActionListener() {
                                public void actionPerformed(ActionEvent e) {
                                    view.SpecificOp2.dispose();
                                    view.SpecificOp2.setVisible(false);

                                }
                            });
                            view.process.addActionListener(new java.awt.event.ActionListener() {
                                public void actionPerformed(ActionEvent e) {

                                    int idOrd = Integer.valueOf(view.addId.getText());
                                    int idCl = Integer.valueOf(view.idCl.getText());
                                    int idProd = Integer.valueOf(view.idPr.getText());
                                    int stoc = Integer.valueOf(view.stock.getText());
                                    Orders ord = new Orders(idOrd, idCl, idProd, stoc);
                                    OrdersBll Ordbll = new OrdersBll();
                                    Ordbll.insertOrder(ord);
                                    view.back.addActionListener(new java.awt.event.ActionListener() {
                                        public void actionPerformed(ActionEvent e) {
                                            view.SpecificOp2.dispose();
                                            view.SpecificOp2.setVisible(false);

                                        }
                                    });
                                }
                            });
                        }
                    });  //
                view.UPDATE.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        view.UpdateProductOperation();
                        view.process.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(ActionEvent e) {

                                int idOrd = Integer.valueOf(view.addId.getText());
                                int idCl = Integer.valueOf(view.idCl.getText());
                                int idProd = Integer.valueOf(view.idPr.getText());
                                int stoc = Integer.valueOf(view.stock.getText());
                                Orders ord = new Orders(idCl, idProd, stoc);
                                OrdersBll Ordbll = new OrdersBll();
                                Ordbll.updateOrders(idOrd, ord);
                                view.back.addActionListener(new java.awt.event.ActionListener() {
                                    public void actionPerformed(ActionEvent e) {
                                        view.SpecificOp2.dispose();
                                        view.SpecificOp2.setVisible(false);

                                    }
                                });
                            }
                        });
                        view.back.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                view.SpecificOp2.dispose();
                                view.SpecificOp2.setVisible(false);

                            }
                        });
                    }
                });
                view.DELETE.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        view.addId.resetKeyboardActions();
                        view.DeleteProductOperation();
                        view.process.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(ActionEvent e) {

                                int id = Integer.valueOf(view.addId.getText());
                                OrdersBll ordBll = new OrdersBll();
                                ordBll.Delete(id);
                                view.back.addActionListener(new java.awt.event.ActionListener() {
                                    public void actionPerformed(ActionEvent e) {
                                        view.SpecificOp2.dispose();
                                        view.SpecificOp2.setVisible(false);
                                    }
                                });
                            }
                        });
                        view.back.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                view.SpecificOp2.dispose();
                                view.SpecificOp2.setVisible(false);

                            }
                        });
                    }
                });
                view.findById.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        view.FindByIdProductOperation();
                        view.process.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(ActionEvent e) {

                                int id = Integer.valueOf(view.addId.getText());
                                OrdersBll ordBll = new OrdersBll();
                                Orders ord = ordBll.findById(id);
                                view.IdAfis(ord.toString());
                                view.back.addActionListener(new java.awt.event.ActionListener() {
                                    public void actionPerformed(ActionEvent e) {
                                        view.SpecificOp3.dispose();
                                        view.SpecificOp3.setVisible(false);
                                    }
                                });

                            }
                        });
                        view.back.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                view.SpecificOp2.dispose();
                                view.SpecificOp2.setVisible(false);

                            }
                        });
                    }
                }); //
                view.showAll.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        view.createTableOrder();
                        view.back.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                view.SpecificOp2.dispose();
                                view.f3.setVisible(false);

                            }
                        });
                    }
                });
                view.back.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        view.ClientiInteraction.dispose();

                        view.ClientiInteraction.setVisible(false);

                    }
                });

             }

        });
        view.Products.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                view.createOptions("Produs");
                PrdTable = true;

                    view.back.addActionListener(new java.awt.event.ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            view.ClientiInteraction.dispose();
                            PrdTable=false;
                            view.ClientiInteraction.setVisible(false);
                        }
                    });
                view.ADD.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        view.AddProductOperation();
                        view.back.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                view.SpecificOp1.dispose();
                                view.SpecificOp1.setVisible(false);

                            }
                        });
                        view.process.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(ActionEvent e) {

                                int id = Integer.valueOf(view.addId.getText());
                                String name = view.addNume.getText();
                                int pret = Integer.valueOf(view.pret.getText());
                                int stoc = Integer.valueOf(view.stock.getText());


                                Product pr = new Product(id, name, pret, stoc);
                                ProductBll prbll = new ProductBll();
                                prbll.insertProduct(pr);
                                view.back.addActionListener(new java.awt.event.ActionListener() {
                                    public void actionPerformed(ActionEvent e) {
                                        view.SpecificOp1.dispose();
                                        view.SpecificOp1.setVisible(false);
                                    }
                                });
                            }
                        });
                    }
                });  //
                view.UPDATE.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        view.UpdateProductOperation();
                        view.process.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(ActionEvent e) {

                                int id = Integer.valueOf(view.addId.getText());
                                String name = view.addNume.getText();
                                int pret = Integer.valueOf(view.pret.getText());
                                int stoc = Integer.valueOf(view.stock.getText());
                                Product pr = new Product(name, pret, stoc);
                                ProductBll prBll = new ProductBll();
                                prBll.updateProduct(id, pr);
                                view.back.addActionListener(new java.awt.event.ActionListener() {
                                    public void actionPerformed(ActionEvent e) {
                                        view.SpecificOp1.dispose();
                                        view.SpecificOp1.setVisible(false);
                                    }
                                });
                            }
                        });
                        view.back.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                view.SpecificOp1.dispose();
                                view.SpecificOp1.setVisible(false);

                            }
                        });
                    }
                });
                view.DELETE.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        view.addId.resetKeyboardActions();
                        view.DeleteProductOperation();
                        view.process.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(ActionEvent e) {

                                int id = Integer.valueOf(view.addId.getText());
                                ProductBll prBll = new ProductBll();
                                prBll.Delete(id);
                                view.back.addActionListener(new java.awt.event.ActionListener() {
                                    public void actionPerformed(ActionEvent e) {
                                        view.SpecificOp1.dispose();
                                        view.SpecificOp1.setVisible(false);
                                    }
                                });
                            }
                        });
                        view.back.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                view.SpecificOp1.dispose();
                                view.SpecificOp1.setVisible(false);
                            }
                        });
                    }
                });
                view.findById.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        view.FindByIdProductOperation();
                        view.process.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(ActionEvent e) {

                                int id = Integer.valueOf(view.addId.getText());
                                ProductBll prBll = new ProductBll();
                                Product afis = prBll.findById(id);
                                view.IdAfis(afis.toString());
                                view.back.addActionListener(new java.awt.event.ActionListener() {
                                    public void actionPerformed(ActionEvent e) {
                                        view.SpecificOp3.dispose();
                                        view.SpecificOp3.setVisible(false);


                                    }
                                });

                            }
                        });
                        view.back.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                view.SpecificOp1.dispose();
                                view.SpecificOp1.setVisible(false);

                            }
                        });
                    }
                }); //
                view.showAll.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        view.createTableProduct();
                        view.back.addActionListener(new java.awt.event.ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                view.f.dispose();
                                view.f.setVisible(false);

                            }
                        });
                    }
                });
            }

        });
    }

}
