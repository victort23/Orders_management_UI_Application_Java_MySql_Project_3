package presentation;

import Bll.ClientBll;
import Bll.OrdersBll;
import Bll.ProductBll;
import model.Client;
import model.Orders;
import model.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.sql.SQLException;
import java.util.ArrayList;

public class View extends JFrame{
    /* JFrame frameRez = new JFrame ("HELLO");
    JFrame frameSim = new JFrame ("Simulation");
    public JPanel p = new JPanel();
    JTextField AdaugaClient = new JTextField(3);
    JLabel nrCT = new JLabel(" Tabel ");



    JPanel panelRez = new JPanel();
    public int OK=0;
    JButton start = new JButton("Start"); */
    JTable table1;
    JTable table2;
    JTable table3;
    DefaultTableModel model;
   /* JButton addClienti;
    JButton updateClienti;
    JButton deleteClient;
    JButton showClienti;
    JButton findByIdClient;
    JButton addOrders;
    JButton updateOrders;
    JButton deleteOrders;
    JButton showOrders;
    JButton findByIdOrder;
    JButton addProduct;
    JButton updateProduct;
    JButton deleteProduct;
    JButton showProducts;
    JButton findByIdProduct; */

   JFrame ClientiInteraction= new JFrame("ClientiOperations");
    JFrame ProductOperations = new JFrame("Product Operations");
    JFrame OrdersOperations = new JFrame("Orders operations");
    JFrame SelectedTable = new JFrame("Select table");
    JFrame SpecificOp = new JFrame("Operation");
    JFrame SpecificOp1 = new JFrame("Operation");
    JFrame SpecificOp2 = new JFrame("Operation");
    JFrame SpecificOp3 = new JFrame("Operation");
    JFrame SpecificOp4 = new JFrame("Operation");
    JFrame f;
    JFrame f1;
    JFrame f3;
    JTextField addId=new JTextField(4);
    JTextField addNume=new JTextField(4);
    JTextField addTelefon=new JTextField(4);
    JTextField addAdresa=new JTextField(4);
    JTextField addEmail=new JTextField(4);
    JTextField addVarsta=new JTextField(4);
    JButton process;
    JButton back;
    JButton ADD=new JButton("Add");
    JButton UPDATE=new JButton("Update");
    JButton DELETE=new JButton("delete");
    JButton showAll=new JButton("ShowAll");
    JButton findById=new JButton("findById");


    JLabel adaugaL;
    JLabel stergeL;
    JLabel updateL;
    JLabel showL;
    JLabel findIdL;
    JButton Clients;
    JButton Orders;
    JButton Products;
    JTextField idCl=new JTextField(4);
    JTextField idPr=new JTextField(4);
    JTextField pret=new JTextField(4);
    JTextField stock=new JTextField(4);
    JPanel afisI=new JPanel();
    public void createTableClient()
    {

        ClientBll clientbll=new ClientBll();
        ArrayList<Client> cl= clientbll.findAll();
        String columns[] = { "IDClient", "NameClient", "Telefon","Adresa","Email","Varsta" };
        String data[][] = new String[cl.size()][6];
        for(int i=0;i<cl.size();i++)
        {
            data[i][0]=String.valueOf(cl.get(i).getId());
            data[i][1]=cl.get(i).getName();
            data[i][2]=cl.get(i).getTelefon();
            data[i][3]=cl.get(i).getAddress();
            data[i][4]=cl.get(i).getEmail();
            data[i][5]=String.valueOf(cl.get(i).getAge());
        }
         model = new DefaultTableModel(data, columns);
        this.table1 = new JTable(model);
        table1.setShowGrid(true);
        JScrollPane pane = new JScrollPane(table1);
        f1 = new JFrame("Populate JTable from Database");
        JPanel panel = new JPanel();
        back=new JButton("Back");
        panel.add(back);
        table1.setShowVerticalLines(true);

        panel.add(pane);
        f1.add(panel);
        f1.setSize(700, 550);
        f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f1.setVisible(true);
    }
    public void createTableProduct()
    {

        ProductBll prodBll=new  ProductBll();
        ArrayList<Product> pr=  prodBll.findAll();
        String columns[] = { "ID Produs", "Nume produs", "Pret","Stock" };
        String data[][] = new String[pr.size()][4];
        for(int i=0;i<pr.size();i++)
        {
            data[i][0]=String.valueOf(pr.get(i).getId());
            data[i][1]=pr.get(i).getName();
            data[i][2]=String.valueOf(pr.get(i).getPrice());
            data[i][3]=String.valueOf(pr.get(i).getStock());

        }
        model = new DefaultTableModel(data, columns);
        this.table2 = new JTable(model);
        table2.setShowGrid(true);
        JScrollPane pane = new JScrollPane(table2);
        f = new JFrame("Populate JTable from Database");
        JPanel panel = new JPanel();
        back=new JButton("Back");
        panel.add(back);
        table2.setShowVerticalLines(true);

        panel.add(pane);
        f.add(panel);
        f.setSize(700, 550);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }
    public void createTableOrder()
    {

       OrdersBll ordBll=new OrdersBll();
        ArrayList<Orders> ord=  ordBll.findAll();
        String columns[] = { "ID Order", "Id Client", "Id produs","Stoc" };
        String data[][] = new String[ord.size()][4];
        for(int i=0;i<ord.size();i++)
        {
            data[i][0]=String.valueOf(ord.get(i).getIdOrder());
            data[i][1]=String.valueOf(ord.get(i).getIdClient());
            data[i][2]=String.valueOf(ord.get(i).getIdProduct());
            data[i][3]=String.valueOf(ord.get(i).getQuantity());
        }
        model = new DefaultTableModel(data, columns);
        this.table3 = new JTable(model);
        table3.setShowGrid(true);
        JScrollPane pane = new JScrollPane(table3);
        f3 = new JFrame("Populate JTable from Database");
        JPanel panel = new JPanel();
        back=new JButton("Back");
        panel.add(back);
        table3.setShowVerticalLines(true);

        panel.add(pane);
        f3.add(panel);
        f3.setSize(700, 550);
        f3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f3.setVisible(true);
    }
    public void createOptions(String obj)
    {
        ClientiInteraction= new JFrame("Operations"+obj);
        ClientiInteraction.setLayout(new GridLayout());
        JPanel panel=new JPanel();
        adaugaL=new JLabel("Adauga "+ obj);
        stergeL=new JLabel("Sterge "+ obj);
        updateL= new JLabel("Update "+ obj);
        showL=new JLabel("Show All "+ obj);
        findIdL=new JLabel("Cauta dupa id "+ obj);
        panel.add(adaugaL);
        panel.add(ADD);
        panel.add(stergeL);
        panel.add(DELETE);
        panel.add(updateL);
        panel.add(UPDATE);
        panel.add(showL);
        panel.add(showAll);
        panel.add(findIdL);
        panel.add(findById);
        back=new JButton("Back");
        panel.add(back);
        ClientiInteraction.add(panel);
        ClientiInteraction.setSize(500, 250);
        ClientiInteraction.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ClientiInteraction.setVisible(true);
    }

    public void TableSelected()
    {
        SelectedTable= new JFrame("SELECT ONE TABLE");
        SelectedTable.setLayout(new GridLayout());
        JPanel panel=new JPanel();
        Clients=new JButton("CLIENTS TABLE");
        Orders=new JButton("ORDERS TABLE");
        Products=new JButton("PRODUCTS TABLE");
        panel.add(Clients);
        panel.add(Orders);
        panel.add(Products);
        SelectedTable.add(panel);
        SelectedTable.setSize(400,200);
                SelectedTable.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SelectedTable.setVisible(true);
    }
    public void AddClientOperation()
    {
        SpecificOp =new JFrame("Adauga client");
        SpecificOp.setLayout(new BorderLayout());
        JPanel newPanel=new JPanel();
        newPanel.add(new JLabel("id: "));
        newPanel.add(addId);
        newPanel.add(new JLabel("nume: "));
        newPanel.add(addNume);
        newPanel.add(new JLabel("adresa: "));
        newPanel.add(addAdresa);
        newPanel.add(new JLabel("telefon: "));
        newPanel.add(addTelefon);
        newPanel.add(new JLabel("email: "));
        newPanel.add(addEmail);
        newPanel.add(new JLabel("varsta: "));
       newPanel.add(addVarsta);
       process=new JButton("Proceseaza adaugarea");
       newPanel.add(process);
        back=new JButton("Back");
        newPanel.add(back);
        SpecificOp.add(newPanel);
        SpecificOp.setSize(600,100);
        SpecificOp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpecificOp.setVisible(true);
    }
    public void AddProductOperation()
    {
        SpecificOp1 =new JFrame("Adauga produs");
        SpecificOp1.setLayout(new BorderLayout());
        JPanel newPanel=new JPanel();
        newPanel.add(new JLabel("id: "));
        newPanel.add(addId);
        newPanel.add(new JLabel("nume : "));
        newPanel.add(addNume);
        newPanel.add(new JLabel("pret: "));
        newPanel.add(pret);
        newPanel.add(new JLabel("Stoc : "));
        newPanel.add(stock);
        process=new JButton("Proceseaza adaugarea");
        newPanel.add(process);
        back=new JButton("Back");
        newPanel.add(back);
        SpecificOp1.add(newPanel);
        SpecificOp1.setSize(600,100);
        SpecificOp1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpecificOp1.setVisible(true);
    }
    public void AddOrderOperation()
    {
        SpecificOp2 =new JFrame("Adauga comanda");
        SpecificOp2.setLayout(new BorderLayout());
        JPanel newPanel=new JPanel();
        newPanel.add(new JLabel("id order: "));
        newPanel.add(addId);
        newPanel.add(new JLabel("id client : "));
        newPanel.add(idCl);
        newPanel.add(new JLabel("id produs: "));
        newPanel.add(idPr);
        newPanel.add(new JLabel("Stoc : "));
        newPanel.add(stock);
        process=new JButton("Proceseaza adaugarea");
        newPanel.add(process);
        back=new JButton("Back");
        newPanel.add(back);
        SpecificOp2.add(newPanel);
        SpecificOp2.setSize(600,100);
        SpecificOp2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpecificOp2.setVisible(true);
    }
    public void UpdateClientOperation()
    {
        SpecificOp =new JFrame("Update client");
        SpecificOp.setLayout(new BorderLayout());
        JPanel newPanel=new JPanel();
        newPanel.add(new JLabel("id-u clientului existent: "));
        newPanel.add(addId);
        newPanel.add(new JLabel("nume: "));
        newPanel.add(addNume);
        newPanel.add(new JLabel("adresa: "));
        newPanel.add(addAdresa);
        newPanel.add(new JLabel("telefon: "));
        newPanel.add(addTelefon);
        newPanel.add(new JLabel("email: "));
        newPanel.add(addEmail);
        newPanel.add(new JLabel("varsta: "));
        newPanel.add(addVarsta);
        process=new JButton("Proceseaza UPDATE");
        newPanel.add(process);
        back=new JButton("Back");
        newPanel.add(back);
        SpecificOp.add(newPanel);
        SpecificOp.setSize(600,100);
        SpecificOp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpecificOp.setVisible(true);
    }
    public void UpdateOrderOperation()
    {
        SpecificOp2 =new JFrame("Update order");
        SpecificOp2.setLayout(new BorderLayout());
        JPanel newPanel=new JPanel();
        newPanel.add(new JLabel("id-u comenzii existente: "));
        newPanel.add(addId);
        newPanel.add(new JLabel("ID client: "));
        newPanel.add(idCl);
        newPanel.add(new JLabel("Id Produs: "));
        newPanel.add(idPr);
        newPanel.add(new JLabel("stocul : "));
        newPanel.add(stock);
        process=new JButton("Proceseaza UPDATE");
        newPanel.add(process);
        back=new JButton("Back");
        newPanel.add(back);
        SpecificOp2.add(newPanel);
        SpecificOp2.setSize(600,100);
        SpecificOp2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpecificOp2.setVisible(true);
    }
    public void UpdateProductOperation()
    {
        SpecificOp1 =new JFrame("Update product");
        SpecificOp1.setLayout(new BorderLayout());
        JPanel newPanel=new JPanel();
        newPanel.add(new JLabel("id-u produsului existent: "));
        newPanel.add(addId);
        newPanel.add(new JLabel("nume: "));
        newPanel.add(addNume);
        newPanel.add(new JLabel("Pret: "));
        newPanel.add(pret);
        newPanel.add(new JLabel("Stoc: "));
        newPanel.add(stock);
        process=new JButton("Proceseaza UPDATE");
        newPanel.add(process);
        back=new JButton("Back");
        newPanel.add(back);
        SpecificOp1.add(newPanel);
        SpecificOp1.setSize(600,100);
        SpecificOp1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpecificOp1.setVisible(true);
    }
    public void DeleteClientOperation()
    {
        SpecificOp =new JFrame("Delete client");
        SpecificOp.setLayout(new BorderLayout());
        JPanel newPanel=new JPanel();
        newPanel.add(new JLabel("Id-ul clientului de sters: "));
        newPanel.add(addId);
        process=new JButton("Proceseaza DELETE");
        newPanel.add(process);
        back=new JButton("Back");
        newPanel.add(back);
        SpecificOp.add(newPanel);
        SpecificOp.setSize(600,100);
        SpecificOp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpecificOp.setVisible(true);
    }
    public void DeleteOrderOperation()
    {
        SpecificOp2 =new JFrame("Delete client");
        SpecificOp2.setLayout(new BorderLayout());
        JPanel newPanel=new JPanel();
        newPanel.add(new JLabel("Id-ul order de sters: "));
        newPanel.add(addId);
        process=new JButton("Proceseaza DELETE");
        newPanel.add(process);
        back=new JButton("Back");
        newPanel.add(back);
        SpecificOp2.add(newPanel);
        SpecificOp2.setSize(600,100);
        SpecificOp2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpecificOp2.setVisible(true);
    }
    public void DeleteProductOperation()
    {
        SpecificOp1 =new JFrame("Delete product");
        SpecificOp1.setLayout(new BorderLayout());
        JPanel newPanel=new JPanel();
        newPanel.add(new JLabel("Id-ul produsului de sters: "));
        newPanel.add(addId);
        process=new JButton("Proceseaza DELETE");
        newPanel.add(process);
        back=new JButton("Back");
        newPanel.add(back);
        SpecificOp1.add(newPanel);
        SpecificOp1.setSize(600,100);
        SpecificOp1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpecificOp1.setVisible(true);
    }
    public void FindByIdClientOperation()
    {
        SpecificOp =new JFrame("Find by Id client");
        SpecificOp.setLayout(new BorderLayout());
        JPanel newPanel=new JPanel();
        newPanel.add(new JLabel("Id-ul clientului de gasit: "));
        newPanel.add(addId);
        process=new JButton("Proceseaza FindById");
        newPanel.add(process);
        back=new JButton("Back");
        newPanel.add(back);
        SpecificOp.add(newPanel);
        SpecificOp.setSize(600,100);
        SpecificOp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpecificOp.setVisible(true);
    }
    public void FindByIdOrderOperation()
    {
        SpecificOp2 =new JFrame("Find by Id client");
        SpecificOp2.setLayout(new BorderLayout());
        JPanel newPanel=new JPanel();
        newPanel.add(new JLabel("Id-ul comenzii de gasit: "));
        newPanel.add(addId);
        process=new JButton("Proceseaza FindById");
        newPanel.add(process);
        back=new JButton("Back");
        newPanel.add(back);
        SpecificOp2.add(newPanel);
        SpecificOp2.setSize(600,100);
        SpecificOp2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpecificOp2.setVisible(true);
    }
    void IdAfis(String txt)
    {
        SpecificOp3 =new JFrame("Find by Id produs");
        SpecificOp3.setLayout(new BorderLayout());
        JPanel pan=new JPanel();
        JLabel jl=new JLabel(txt);
        back=new JButton("Back");
        pan.add(jl);
        pan.add(back);
        SpecificOp3.add(pan);
        SpecificOp3.setSize(600,100);
        SpecificOp3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpecificOp3.setVisible(true);
    }
    public void FindByIdProductOperation()
    {
        SpecificOp1 =new JFrame("Find by Id produs");
        SpecificOp1.setLayout(new BorderLayout());
        afisI=new JPanel();
        afisI.add(new JLabel("Id-ul produsului de gasit: "));
        afisI.add(addId);
        process=new JButton("Proceseaza FindById");
        afisI.add(process);
        back=new JButton("Back");
        afisI.add(back);
        SpecificOp1.add(afisI);
        SpecificOp1.setSize(600,400);
        SpecificOp1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpecificOp1.setVisible(true);
    }




}
