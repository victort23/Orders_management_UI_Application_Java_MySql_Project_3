package Bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


import Bll.validators.Validator;
import DAO.ClientDAO;
import DAO.ProductDAO;
import model.Client;
import model.Product;

public class ProductBll {

    private List<Validator<Product>> validators;

    public ProductBll() {
        validators = new ArrayList<Validator<Product>>();
        // validators.add(new EmailValidator());
        //validators.add(new ClientAgeValidators());
    }

    public Product findById(int id) {
        Product st = ProductDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The product with id =" + id + " was not found!");
        }
        return st;
    }
    public ArrayList<Product> findAll() {
        ArrayList<Product> cl = ProductDAO.findAll();
        if (cl.isEmpty()==true) {
            throw new NoSuchElementException("There are no clients");
        }
        return cl;
    }
    public int insertProduct(Product client) {
        for (Validator<Product> v : validators) {
            v.validate(client);
        }
        return ProductDAO.insert(client);
    }
    public int updateProduct(int id,Product prod) {
        for (Validator<Product> v : validators) {
            v.validate(prod);
        }
        Product st = ProductDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The product with id =" + id + " was not found!");
        }
        else
            return ProductDAO.Update(id,prod);
    }
    public int Delete(int id)
    {
        Product st = ProductDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The product with id =" + id + " was not found!");
        }
        else
            return ProductDAO.Delete(id);
    }
}
