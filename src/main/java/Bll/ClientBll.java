package Bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import Bll.validators.EmailValidator;
import Bll.validators.ClientAgeValidators;
import Bll.validators.Validator;
import DAO.ClientDAO;
import model.Client;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class ClientBll {

    private List<Validator<Client>> validators;

    public ClientBll() {
        validators = new ArrayList<Validator<Client>>();
        validators.add(new EmailValidator());
        validators.add(new ClientAgeValidators());
    }

    public Client findClientById(int id) {
        Client st = ClientDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The student with id =" + id + " was not found!");
        }
        return st;
    }
    public ArrayList<Client> findAll() {
        ArrayList<Client> cl = ClientDAO.findAll();
        if (cl.isEmpty()==true) {
            throw new NoSuchElementException("There are no clients");
        }
        return cl;
    }
    public int insertClient(Client client) {
        for (Validator<Client> v : validators) {
            v.validate(client);
        }
        return ClientDAO.insert(client);
    }
    public int updateClient(int id,Client client) {
        for (Validator<Client> v : validators) {
            v.validate(client);
        }
        Client st = ClientDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The student with id =" + id + " was not found!");
        }
            return ClientDAO.Update(id,client);
    }
    public int Delete(int id)
    {
        Client st = ClientDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The student with id =" + id + " was not found!");
        }
            return ClientDAO.Delete(id);
    }
}
