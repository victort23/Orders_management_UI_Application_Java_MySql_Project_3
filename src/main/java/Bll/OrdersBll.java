package Bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import Bll.validators.Validator;
import DAO.ClientDAO;
import DAO.OrdersDAO;
import model.Client;
import model.Orders;
public class OrdersBll {

    private List<Validator<Orders>> validators;

    public OrdersBll() {
        validators = new ArrayList<Validator<Orders>>();
        // validators.add(new EmailValidator());
        //validators.add(new ClientAgeValidators());
    }

    public Orders findById(int id) {
        Orders ord = OrdersDAO.findById(id);
        if (ord == null) {
            throw new NoSuchElementException("The order with id =" + id + " was not found!");
        }
        return ord;
    }
    public ArrayList<Orders> findAll() {
        ArrayList<Orders> ord = OrdersDAO.findAll();
        if (ord.isEmpty()==true) {
            throw new NoSuchElementException("There are no orders");
        }
        return ord;
    }
    public int insertOrder(Orders ord) {
        for (Validator<Orders> v : validators) {
            v.validate(ord);
        }
        return OrdersDAO.insert(ord);
    }
    public int updateOrders(int id,Orders ord) {
        for (Validator<Orders> v : validators) {
            v.validate(ord);
        }
        Orders ordd = OrdersDAO.findById(id);
        if (ordd == null) {
            throw new NoSuchElementException("The order with id =" + id + " was not found!");
        }
        else
            return OrdersDAO.Update(id,ord);
    }
    public int Delete(int id)
    {
        Orders ord =OrdersDAO.findById(id);
        if (ord == null) {
            throw new NoSuchElementException("The order with id =" + id + " was not found!");
        }
            return OrdersDAO.Delete(id);
    }
}
